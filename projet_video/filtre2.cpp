#include <cstdio>
#include <sstream>
#include <iomanip>
#include "filtre2.h"

/***************************************************************************
 *      SC_THREAD d'enregistrement des donnés
 ***************************************************************************/
void FILTRE2::buf_entrees()
{
  if(reset_n == false)
    {
      start = 0;
      cpt_in = 0;
      
      pixel_out = 0;
      href_out = false;
      vref_out = false;
      
      tab = new unsigned char [720*576];
      
      cout << "module: " << name() << "... reset!" << endl;
    }
  
  // boucle infinie
  while(1)
    {
      wait();
      
      //remplissage de la matrice
      if (href_in)
	{
	  int line = cpt_in / WIDTH;
	  int column = cpt_in % WIDTH;

	  if ( (( (HEIGHT/4) <= line ) && ( line < (3*HEIGHT/4) ))
	       && ( ( (WIDTH/4) <= column ) && ( column < (3*WIDTH/4) ) ) )
	    {
	      tab[2*WIDTH*(line-HEIGHT/4)+2*(column-WIDTH/4)] = pixel_in;
	      tab[2*WIDTH*(line-HEIGHT/4)+2*(column+1-WIDTH/4)] = pixel_in;
	      tab[2*WIDTH*(line+1-HEIGHT/4)+2*(column-WIDTH/4)] = pixel_in;
	      tab[2*WIDTH*(line+1-HEIGHT/4)+2*(column+1-WIDTH/4)] = pixel_in;
	    }
	  
	  cpt_in++;	  
	  if(cpt_in == WIDTH * HEIGHT) cpt_in = 0;
	}
      
      //1/4 de tableau d'avance
      cpt_out = (WIDTH * HEIGHT *3/4 + cpt_in) % (WIDTH * HEIGHT);
      
      //départ des signaux
      if (cpt_in == HEIGHT * WIDTH / 4 ) start = 1;
      
      //condition indispensable pour eviter la segmentation fault
      if (start) pixel_out = tab[cpt_out];
      href_out = href_in && start;
      
      if ((0 <= cpt_out) && (cpt_out < 3)) vref_out = 1;
      else vref_out = 0;
    }
  delete[] tab;
}
