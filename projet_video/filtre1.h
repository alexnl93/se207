#ifndef FILTRE1_H
#define FILTRE1_H

#define WIDTH 720
#define HEIGHT 576

#include <systemc.h>
#include "image.h"

/***************************************
 *  définition du module
 **************************************/
SC_MODULE(FILTRE1) {

   // IO PORTS
   sc_in<bool>         clk;
   sc_in<bool>         reset_n;

   sc_in<bool>        href_in;
   sc_in<bool>        vref_in;
   
   sc_out<bool>        href_out;
   sc_out<bool>        vref_out;

   sc_in<unsigned char> pixel_in;

   sc_out<unsigned char> pixel_out;

   int cpt_in;// numéro de pixel courant
   int cpt_out;

   bool start;
   
   /***************************************************
    *  constructeur
    **************************************************/
   SC_CTOR(FILTRE1):base_name("filtre1")
   {
      cout << "Instanciation de " << name() <<" ..." ;

      SC_THREAD (buf_entrees);
      sensitive << clk.pos();
      async_reset_signal_is(reset_n,false);
      dont_initialize();

      //appelle de la fonction à la fin du thread

      cout << "... réussie" << endl;
   }

   /***************************************************
    *  méthodes et champs internes
    **************************************************/
   private:

   void buf_entrees();
   unsigned char calc();

   const std::string   base_name;              // nom de base des images d'entrée
   unsigned char * tab;
   
};

#endif
