#include <cstdio>
#include <sstream>
#include <iomanip>
#include "video_out.h"

/***************************************************************************
 *      SC_THREAD principale
 ***************************************************************************/
void VIDEO_OUT::aff_entrees()
{
  if(reset_n == false)
    {
      // Reset : on remet tous les paramètres à  zéro
      if(current_image_number !=0)
	{
	  current_image_number = 0;
	}

      cpt = 0;
      tab = new unsigned char [720*576];
      cout << "module: " << name() << "... reset!" << endl;
    }
  
  // boucle infinie
  while(1)
    {
      // pour chaque ecriture href doit être à 1
      wait();
      if (href)
	{
	  
	  tab[cpt++] = pixel_in;
	 
	  if(cpt == image.width * image.height)
	    {
	      image.pixel = tab;

	      
	      std::cout << "Image produite" << endl;
	      //On créer l'autre image
	      write_image();
	      delete[] tab;
	      tab = new unsigned char [720*576];
	      // On a fini une image, on passe à la suivante
	      current_image_number ++;
	      cpt = 0;
	    }
	}
    }
}

/****************************************
 *  Lectures des images successives
 ***************************************/

void VIDEO_OUT::write_image()
{
   std::ostringstream name_s;

   // Calcul du nom de la prochaine image.
   name_s << base_name << std::setfill('0') << std::setw(2) << current_image_number << ".png";
   
   //TODO a supprimer si ce message d'erreur ne s'affiche pas
   // L'image est lue et chargée en mémoire.
   // On vérifie quand même que l'image lue a la bonne taille (720*576)
   if((image.width != 720) || (image.height != 576))
     {
       std::cerr << name() << " l'image " << name_s.str() << " n'a pas les bonnes dimensions (720*576)" << endl;
       exit(-1);
     }
   
   // Ecriture de l'image PNG a l'aide de la libpng
   std::cout << name() << " Création de l'image " << name_s.str() << endl;
   image_write(&image, name_s.str().c_str());

}
