#ifndef VIDEO_OUT_H
#define VIDEO_OUT_H

#include <systemc.h>
#include "image.h"

/***************************************
 *  définition du module
 **************************************/
SC_MODULE(VIDEO_OUT) {

   // IO PORTS
   sc_in<bool>         clk;
   sc_in<bool>         reset_n;

   sc_in<bool>        href;
   sc_in<bool>        vref;

   sc_in<unsigned char> pixel_in;

   int cpt;// numéro de pixel courant
   /***************************************************
    *  constructeur
    **************************************************/
   SC_CTOR(VIDEO_OUT):base_name("wallace_modifier")
   {
      cout << "Instanciation de " << name() <<" ..." ;

      SC_THREAD (aff_entrees);
      sensitive << clk.pos();
      async_reset_signal_is(reset_n,false);
      dont_initialize();

      current_image_number = 0;
      image.pixel = NULL;
      image.width = 720;
      image.height = 576;
      
      //appelle de la fonction à la fin du thread

      cout << "... réussie" << endl;
   }

   /***************************************************
    *  méthodes et champs internes
    **************************************************/
   private:

   void aff_entrees();
   void write_image();

   const std::string   base_name;              // nom de base des images d'entrée
   int                 current_image_number;   // numéro de l'image courante
   
   unsigned char * tab;

   Image               image;

};

#endif
