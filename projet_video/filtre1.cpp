#include <cstdio>
#include <sstream>
#include <iomanip>
#include "filtre1.h"

/***************************************************************************
 *      SC_THREAD d'enregistrement des donnés
 ***************************************************************************/
void FILTRE1::buf_entrees()
{
  if(reset_n == false)
    {
      start = 0;
      cpt_in = 0;
      
      pixel_out = 0;
      href_out = false;
      vref_out = false;
      
      tab = new unsigned char [720*576];
      
      cout << "module: " << name() << "... reset!" << endl;
    }
  
  // boucle infinie
  while(1)
    {
      // pour chaque ecriture href doit être à 1
      wait();
      
      if (href_in)
	{  
	  tab[cpt_in++] = pixel_in;
	  if(cpt_in == WIDTH * HEIGHT) cpt_in = 0; 
	}

      //3 pour une avance de trois lignes
      cpt_out = (WIDTH * HEIGHT + cpt_in - 3 * WIDTH) % (WIDTH * HEIGHT);

      //départ des signaux
      if (cpt_in == WIDTH*3 ) start = 1;

      //condition indispensable pour eviter la segmentation fault
      if (start) pixel_out = calc();
      href_out = href_in && start;
      if ((0 <= cpt_out) && (cpt_out < 3*WIDTH)) vref_out = 1;
      else vref_out = 0;
    }
  delete[] tab;
}

/****************************************
 *  calcul des valeurs en fonction de la position
 ***************************************/

unsigned  char FILTRE1::calc()
{
  int line  = cpt_out / WIDTH;
  int column = cpt_out % WIDTH;

  int min_line, min_column, max_line, max_column;
  unsigned int res = 0;

  switch (line) {
  case 0:
    min_line = 1;
    max_line = 2;
    break;

  case HEIGHT:
    min_line = 0;
    max_line = 1;
    break;

  default:
    min_line = 0;
    max_line = 2;
    break;
  }
  
  switch (column) {
  case 0:
    min_column = 1;
    max_column = 2;
    break;

  case WIDTH:
    min_column = 0;
    max_column = 1;
    break;

  default:
    min_column = 0;
    max_column = 2;
    break;
  }

  int nb_value = 0;
  
  for (int i=min_line; i <= max_line; i++){
    for (int j=min_column; j <= max_column; j++){
      nb_value++;
      res = res + tab[cpt_out + (j -1) + (i - 1)*WIDTH];
    }
  }

  res = res / nb_value;
  return res;
}
