#include <systemc.h>

//func de base à appeler
sc_uint<8> pgcd_func(sc_uint<8> a, sc_uint<8> b)
{
  sc_uint<8> max = a>b? a : b;
  sc_uint<8> min = a>b? b : a;

  while(max != min)
    {
      sc_uint<8> d = max - min;
      max = min>d? min : d;
      min = min>d? d : min;
    }
    
  return max;
      
}

SC_MODULE(PGCD_SOFT){

  sc_in<bool> clk;
  sc_in<sc_uint<8> > a,b;
  sc_in<bool> valid;
  sc_out<bool> ready;
  sc_out<sc_uint<8> > res;
  
  SC_CTOR(PGCD_SOFT): clk("clk"), valid("valid"), ready("ready"), a("a"), b("b"), res("res")
  {
    SC_THREAD(pgcd_soft2);
    sensitive << clk.pos();
  }

  //pas de detail version 1
  void pgcd_soft1(){
     while(1){
        while(!valid) wait();
        res = pgcd_func(a,b);
        ready = 1;
        wait();
        ready = 0;
     }
  }

  
  // detail version 2
  void pgcd_soft2(){
    while(1){
      while(!valid) wait();

      {
	sc_uint<8> max = a>b? a : b;
	sc_uint<8> min = a>b? b : a;

	while(max != min)
	  {
	    sc_uint<8> d = max - min;
	    max = min>d? min : d;
	    min = min>d? d : min;
	    res = max;
	    wait();
	  }
      
      }
      ready = 1;
      wait();
      ready = 0;
    }
  }
  
};

SC_MODULE(PGCD_HARD){

  sc_in<bool> clk;
  sc_in<sc_uint<8> > a,b;
  sc_in<bool> valid;
  sc_out<bool> ready;
  sc_out<sc_uint<8> > res;
  sc_in<bool> rst;
  
  sc_signal<bool> calcul;
  sc_uint<8> max,min;
  sc_uint<8> diff;
  
  SC_CTOR(PGCD_HARD) : clk("clk"), valid("valid"), ready("ready"), a("a"), b("b"), res("res"), rst("rst")
  {
    SC_METHOD(estate);
    sensitive << clk.pos();

    SC_METHOD(pgcd_hard);
    sensitive << clk.pos();
  }
  
  void pgcd_hard(){

     if(valid)
     {
        max = a>b? a : b;
        min = a>b? b : a;
     }
     else
     {
        diff = max - min;
        max  = min>diff? min  : diff;
        min  = min>diff? diff : min;
     }


     res = max;  
  }

  void estate(){


     if(rst)
     {
        calcul = false;
        ready = 0;
     }
     else
     {
        ready = 0;
        if (valid){
           calcul = true;
        }
        if (diff == 0  && calcul){
           ready = 1;
           calcul = false;
        }
     }
  }
  
};


int sc_main (int argc, char * argv[])
{
  // Un pointeur sur l'objet qui permet de gérer les traces
  sc_trace_file *trace_f;
  trace_f = sc_create_vcd_trace_file ("my_simu_trace");
  trace_f->set_time_unit(1,SC_NS);
  
  sc_signal<sc_uint<8> > a,b,res;
  sc_signal<bool> valid, ready, rst;
  sc_clock clk("clk",10,SC_NS);

  sc_trace(trace_f,a, "a");
  sc_trace(trace_f,b, "b");
  sc_trace(trace_f,res, "resultat");
  sc_trace(trace_f,clk, "clk");
  sc_trace(trace_f,ready, "ready");
  sc_trace(trace_f,rst, "rst");
  sc_trace(trace_f,valid, "valid_inter");

  PGCD_SOFT my_PGCD("processus_pgcd");
  my_PGCD.a(a);
  my_PGCD.b(b);
  my_PGCD.res(res);
  my_PGCD.valid(valid);
  my_PGCD.ready(ready);
  my_PGCD.clk(clk);
  //commenter cette ligne pour
  //passer sur le pgcd soft
  // my_PGCD.rst(rst);

  a = 15;
  b = 6;
  valid = 0;
  rst = 0;
  sc_start(20,SC_NS);
  
  rst = 1;
  sc_start(20,SC_NS);

  rst = 0;
  sc_start(20,SC_NS);  
  
  valid = 1;
  sc_start(10,SC_NS);
  
  valid = 0;
  sc_start(60,SC_NS);
  
  a = 60;
  b = 45;
  valid = 1;
  sc_start(10,SC_NS);
  
  valid = 0;
  sc_start(200,SC_NS);

  a = 15;
  b = 6;
  valid = 0;
  sc_start(20,SC_NS);
  
  valid = 1;
  sc_start(10,SC_NS);
  
  valid = 0;
  sc_start(60,SC_NS);
  
  sc_close_vcd_trace_file(trace_f);
  return 0;
}
