#include <systemc.h>

// un type utilisateur
struct Pixel {
  sc_uint<5> R;
  sc_uint<6> G;
  sc_uint<5> B;
  
  // un constructeur particulier avec des valeurs par défaut
  Pixel( sc_uint<5> r=0, sc_uint<6> g=0, sc_uint<5> b=0): R(r), G(g), B(b) { }

  bool operator == (const Pixel &other) {
    return (R == other.R) && (G == other.G) && (B == other.B);
  }

  bool test_odd_sum (){
    sc_uint<5> res = R+G+B;
    //on test si c'est pair ou pas
    return(res%2==0);
  }
  
  // Comment ce type est imprimé
  // l'opérateur << est un opérateur de la classe std::ostream
  friend ostream& operator << ( ostream& o, const Pixel& P ) {
    o << "{" << P.R << "," << P.G << "," << P.B << "}" ;
    return o;
  }
};

// surcharge de la fonction sc_trace pour le type utilisateur
void sc_trace( sc_trace_file* _f, const Pixel& _foo, const std::string& _s ) {
  sc_trace( _f, _foo.R, _s + "r" );
  sc_trace( _f, _foo.G, _s + "g" );
  sc_trace( _f, _foo.B, _s + "b" );
}

SC_MODULE(MODULE_A){

  sc_in<bool> clk;
  sc_in<bool> rst;
  sc_fifo_out<Pixel> fifo;

  int cpt1;
  unsigned int cpt2;
  bool write;
  
  SC_CTOR(MODULE_A): clk("clk"), rst("rst"), fifo("fifo")
    {
      //    write = false;
      SC_METHOD(send_hard);
      sensitive << clk.pos();
    }
  
  //version method
  void send_hard(){
    if(rst){
      cpt1 = 100;
      cpt2 = rand()%32;
      write = true;
      cout << "reset here" << endl;
    }else{
      if (write){
        
	if(fifo.num_free()!=0){
	  Pixel tmp(rand(),rand(),rand());
	  cout << "sending " <<  tmp << " @ " << sc_time_stamp() << endl;
	  fifo.write(tmp);
	  cpt1--;
	}
	if (cpt1 == 0) write = false;
      } else {
	cpt2--;
	if (cpt2 == 0) {
	  cpt1 = 100;
	  cpt2 = rand()%32;
	  write = true;
	}
      }
    }		       
  }
  
  //version thread
  void send_soft(){
    while(1){
      cpt1 = 100;
      cpt2 = rand()%32;

      wait();

      while(cpt1 > 0) {
	Pixel tmp(rand(),rand(),rand());
	cout << "sending " <<  tmp << " @ " << sc_time_stamp() << endl;
	fifo.write(tmp);
	write = !write;
	cpt1--;
	wait();
      }

      while(cpt2 > 1) {
	cpt2--;
	wait();
      }

        cpt1 = 100;
        cpt2 = rand();

     }
  }
  
};

SC_MODULE(MODULE_B){

  sc_in<bool> clk;
  sc_in<bool> rst;
  sc_fifo_in<Pixel> fifo;

  int cpt;
  bool read;
  
  SC_CTOR(MODULE_B): clk("clk"), fifo("fifo")
    {
      read = false;
      SC_THREAD(received);
      sensitive << clk.pos();
    }

   void received(){
      while(1){

         wait();      

         //on lit un élément de la fifo
         Pixel buf = fifo.read();
         cout
            << "received " <<  buf << " @ " << sc_time_stamp() << endl
            << fifo->num_available() << " left in fifo" << endl;

         read = !read;

         if(buf.test_odd_sum()) cpt = 2;
         else cpt = 3;

         //on est dans le cycle d'attente
         while(cpt > 1){
            cpt--;
            wait();
         }
      }
   }
  
};

// Le test
int sc_main (int argc, char * argv[])
{
  // Un pointeur sur l'objet qui permet de gérer les traces
  sc_trace_file *trace_f;
  trace_f = sc_create_vcd_trace_file ("my_simu_trace");
  trace_f->set_time_unit(100,SC_NS);

  sc_clock clk1("clk1",33,SC_NS);
  sc_clock clk2("clk2",50,SC_NS);
  sc_fifo<Pixel> fifo(10);
  sc_signal<bool> rst;

   MODULE_A a("modA");
   a.clk(clk1);
   a.rst(rst);
   a.fifo(fifo);

   MODULE_B b("modB");
   b.clk(clk2);
   b.rst(rst);
   b.fifo(fifo);
  
   sc_trace(trace_f,clk1, "clk1");
   sc_trace(trace_f,clk2, "clk2");
   sc_trace(trace_f,rst, "rst");
   //sc_trace(trace_f,fifo, "fifo"); //interdit
   sc_trace(trace_f,b.read, "read");
   sc_trace(trace_f,a.write, "write");
   sc_trace(trace_f,a.cpt1, "cpt1");

  rst = 0;
  sc_start(20,SC_US);
  
  rst = 1;
  sc_start(20,SC_US);

  rst = 0;
   sc_start(1,SC_MS);

   sc_close_vcd_trace_file(trace_f);
   return 0;
}

