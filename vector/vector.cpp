#include <systemc.h>

int sc_main (int argc, char * argv[])
{
  sc_bv<8> x;
  sc_lv<8> y;

  x = 44;
  // Imprimera 44
  cout << "x---> " << x  << "(" << x.to_uint() << ")" << endl;

  // Imprimera 44 aussi car x est sur 8 bits
  x = 300;
  cout << "x---> " << x  << "(" << x.to_uint() << ")" << endl;

  x[0] = false;
  sc_bit b;
  b = x[1];

  x(7,4) = "1010";
  x(3,0) = x(7,4);

  cout << "x---> " << x  << "(" << x.to_uint() << ")" << endl;
    
  cout << "hello world" << endl;

   return 0;
}
