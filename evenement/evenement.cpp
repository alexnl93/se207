#include <systemc.h>

// un type utilisateur
struct pixel {
  sc_uint<5> R;
  sc_uint<6> G;
  sc_uint<5> B;
  
  // un constructeur particulier avec des valeurs par défaut
  pixel( sc_uint<5> r=0, sc_uint<6> g=0, sc_uint<5> b=0): R(r), G(g), B(b) { }

  bool operator == (const pixel &other) {
    return (R == other.R) && (G == other.G) && (B == other.B);
  }
  // Comment ce type est imprimé
  // l'opérateur << est un opérateur de la classe std::ostream
  friend ostream& operator << ( ostream& o, const pixel& P ) {
    o << "{" << P.R << "," << P.G << "," << P.B << "}" ;
    return o;
  }

};


///bloc combinatoire d'addition numéro 1
SC_MODULE(comb_add1){
  sc_in<pixel> P1, P2;
  sc_out<pixel > PO;

  SC_CTOR(comb_add1)
  {
    // La méthode bar est sensible aux évènement sur a et b
    SC_METHOD(addrg);
    sensitive << P1 << P2 ;
    // attendre le premier évènement avant de faire le premier calcul
    dont_initialize();
  }

  void addrg(){
    sc_uint<6> resR = P1.read().R + P2.read().R;
    sc_uint<7> resG = P1.read().G + P2.read().G;
    sc_uint<6> resB = P1.read().B + P2.read().B;

    pixel buf = pixel(resR,resG,resB);
    PO.write(buf);
  }
};

// surcharge de la fonction sc_trace pour le type utilisateur
void sc_trace( sc_trace_file* _f, const pixel& _foo, const std::string& _s ) {
   sc_trace( _f, _foo.R, _s + "r" );
   sc_trace( _f, _foo.G, _s + "g" );
   sc_trace( _f, _foo.B, _s + "b" );
}

// Le test
int sc_main (int argc, char * argv[])
{
  // Un pointeur sur l'objet qui permet de gérer les traces
  sc_trace_file *trace_f;
  trace_f = sc_create_vcd_trace_file ("my_simu_trace");
  trace_f->set_time_unit(1,SC_NS);

  //resultat de l'addition saturante deux bit avec le 1 er pointeur
  sc_signal<pixel> P1;
  sc_signal<pixel> P2;
  sc_signal<pixel> PO;
  
  sc_trace(trace_f,P1, "P1");
  sc_trace(trace_f,P2, "P2");
  sc_trace(trace_f,PO, "P1O");
  
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;

  // affectation au signal
  comb_add1 add1("bloc_comb_add1");
  add1.P1(P1);
  add1.P2(P2);
  add1.PO(PO);
   
  P1 = pixel(33,22,45);
  P2 = pixel(21,10,32);
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;

  sc_start(1,SC_NS);
  P1 = pixel(33,22,45);
  P2 = pixel(21,10,32);
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;

  P1 = pixel(23,22,40);
  P2 = pixel(63,10,32);
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;

  sc_start(1,SC_NS);
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;

  P1 = pixel(34,22,10);
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;

  sc_start(1,SC_NS);
  cout << "--> @ " << sc_time_stamp() << " P1 = " << P1 << endl;
  cout << "--> @ " << sc_time_stamp() << " P2 = " << P2 << endl;
  cout << "--> @ " << sc_time_stamp() << " PO = " << PO << endl;
   
  sc_close_vcd_trace_file(trace_f);
  return 0;
}

